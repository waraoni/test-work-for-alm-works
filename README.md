# Test work for ALM Works

It took me ~7 hours to complete the task. But it's my first experience with React, meaning that part of the time I've spent on reading documentation and trial and error.

Appication is very simple. It has StickyNotes component that does most of the work. Could have actually placed the code of StickyNotes into App.js. But just happened to leave App.js untouched until the very last moment.

StickyNotes loads and saves notes from localstorage using Storage helper class. Each StickyNote has it's size, position, id, content (text) and color of the background. Can be resized or dragged. Clicking on a StickyNote brings it to the front. And a StickyNote gets deleted upon being dragged into DeleteArea component. That's the general idea.

Dragging and resizing of a StickyNote is implemented via requestAnimationFrame to reach the best performance (in comparance to using React state to change position and size of a note).
### Clone repository and run `npm start`, then open [http://localhost:3000](http://localhost:3000) to test it
