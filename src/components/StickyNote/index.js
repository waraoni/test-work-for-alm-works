import { Component, createRef } from 'react';
import './index.css';

class StickyNote extends Component {
  constructor (props) {
    super(props);

    this.body = document.querySelector('body');
    this.noteRef = createRef();
    this.frameHandler = null;
    this.dragMode = null;
    this.dragStartedAtX = 0;
    this.dragStartedAtY = 0;

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.onTextInputHandler = this.onTextInputHandler.bind(this);
  }

  handleMouseDown (event) {
    // onClick we bring the note to front
    this.props.onClick instanceof Function && this.props.onClick();

    // EventListeners get attached to the body so we need it
    if (this.body instanceof HTMLElement) {
      this.dragStartedAtX = event.pageX;
      this.dragStartedAtY = event.pageY;

      // Do we drag or resize the note?
      this.dragMode = (
        event.pageX >= this.props.model.x + this.props.model.width - 16 &&
        event.pageY >= this.props.model.y + this.props.model.height - 16
      )
        ? 'resize'
        : 'drag';

      if (this.dragMode === 'drag') {
        // Use requestAnimationFrame to move notes – better performance
        requestAnimationFrame(() => this.noteRef.current.style.cursor = 'move');

        // Notify the parent component that the note has started to be dragged
        this.props.onStaredDragging instanceof Function && this.props.onStaredDragging(this.props.model);
      }

      this.body.addEventListener('mouseup', this.handleMouseUp);
      this.body.addEventListener('mousemove', this.handleMouseMove);
    }
  }

  handleMouseMove (event) {
    const _this = this,
          note = _this.noteRef.current;

    event && event.preventDefault();
    
    // Draw dragging or resizing of the note
    cancelAnimationFrame(this.frameHandler);
    this.frameHandler = requestAnimationFrame(function () {
      if (!note) return;
      switch (_this.dragMode) {
        case 'drag':
          note.style.transform = `translate3d(
            ${event.pageX - _this.dragStartedAtX}px,
            ${event.pageY - _this.dragStartedAtY}px,
            0
          )`;
          // Notify the parent component where is the cursor at the moment
          // to calculate if the note in DeleteArea
          _this.props.onDragging instanceof Function && _this.props.onDragging(event.pageX, event.pageY);
          break;

        case 'resize':
          note.style.width = `${Math.max(100, _this.props.model.width + event.pageX - _this.dragStartedAtX)}px`;
          note.style.height = `${Math.max(100, _this.props.model.height + event.pageY - _this.dragStartedAtY)}px`;
          break;

        default:
      }
    })
  }

  handleMouseUp (event) {
    var _this = this;
    event && event.preventDefault();
    
    // Remove EventListeners from body
    if (this.body instanceof HTMLElement) {
      this.body.removeEventListener('mouseup', this.handleMouseUp);
      this.body.removeEventListener('mousemove', this.handleMouseMove);
    }

    cancelAnimationFrame(this.frameHandler);
    this.frameHandler = requestAnimationFrame(function () {
      if (!_this.noteRef.current) return;

      // Remove custom styles
      _this.noteRef.current.style.transform = ``
      _this.noteRef.current.style.cursor = ``;
    });

    if (this.props.onUpdate instanceof Function) {
      // Update the note
      switch (_this.dragMode) {
        case 'drag':
          this.props.onUpdate(Object.assign({}, this.props.model, {
            x: this.props.model.x + event.pageX - this.dragStartedAtX,
            y: this.props.model.y + event.pageY - this.dragStartedAtY,
          }));
          this.props.onEndedDragging instanceof Function && this.props.onEndedDragging(this.props.model);
          break;

        case 'resize':
          this.props.onUpdate(Object.assign({}, this.props.model, {
            width: Math.max(100, _this.props.model.width + event.pageX - _this.dragStartedAtX),
            height: Math.max(100, _this.props.model.height + event.pageY - _this.dragStartedAtY),
          }));
          break;

        default:
      }
    }

    this.dragMode = null;
  }

  onTextInputHandler () {
    // Watch for note content changes
    if (this.props.onUpdate instanceof Function && this.noteRef.current) {
      this.props.onUpdate(Object.assign({}, this.props.model, {
        content: this.noteRef.current.value
      }));
    }
  }

  componentWillUnmount () {
    // Make sure we remove EventListeners from body
    if (this.isDragging && this.body instanceof HTMLElement) {
      this.handleMouseUp();
    }
  }

  render () {
    if (this.props.model.focus) {
      // Focus textarea if needed
      setTimeout(() => {
        this.props.onUpdate(Object.assign({}, this.props.model, {
          focus: false
        }));
        this.noteRef.current.focus()
      })
    }

    return (
      <textarea
        className={`StickyNote${this.props.isReadyToDelete ? ' StickyNote--is-ready-to-delete' : ''}`}
        style={
          {
            backgroundColor: 'isOutlined' in this.props ? '' : this.props.model.color,
            border: 'isOutlined' in this.props ? `2px dashed ${this.props.model.color}` : '',
            left: `${this.props.model.x}px`,
            top: `${this.props.model.y}px`,
            width: `${this.props.model.width}px`,
            height: `${this.props.model.height}px`,
            zIndex: `${this.props.model.zIndex + 1}`
          }
        }
        onMouseDown={this.handleMouseDown}
        onInput={this.onTextInputHandler}
        ref={this.noteRef}
        value={'isOutlined' in this.props ? '' : this.props.model.content}
      ></textarea>
    );
  }
}

export default StickyNote;