
import { Component } from 'react';
import './index.css';

class DeleteArea extends Component {
  render () {
    return (
      <div
        className={`DeleteArea${this.props.isShown ? ' DeleteArea--is-shown' : ''}${this.props.isReadyToDelete ? ' DeleteArea--is-ready-to-delete' : ''}`}
        style={{ zIndex: this.props.zIndex - 1 }}
      >
        <div className="DeleteArea__caption">To remove a Sticky Note simply drag'n'drop it into this area</div>
      </div>
    );
  }
}

export default DeleteArea;