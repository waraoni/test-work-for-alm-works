
import { Component } from 'react';
import './index.css';
import StickyNote from './../StickyNote';
import DeleteArea from './../DeleteArea';
import Storage from './../../storage';
import API from './../../api';
import Color from './../../util/color';

class StickyNotes extends Component {
  static createNote (x, y, width, height) {
    const id = `f${Date.now().toString(16)}`,
          color = Color.generatePseudoRandom();

    return { id, x, y, width, height, color, content: '' }
  }

  constructor (props) {
    super(props);
    this.state = {
      notes: Storage.load('stickyNotes', []), // Load notes
      isCreateNoteModeActive: false,
      newStickyNote: null,
      noteThatIsBeingDragged: null,
      canDeleteNote: false,
      isAnimatingAddNoteBtn: true,
      isSavingOnApi: false,
      isLoadingFromApi: false
    };

    this.isDrawingANote = false;
    this.dragStartedAtX = 0;
    this.dragStartedAtY = 0;
  }

  /**
   * Orders Sticky Notes using z-index
   * 
   * @param {Array<{
   *  id: string,
   *  x: number,
   *  y: number,
   *  width: number,
   *  height: number,
   *  zIndex?: number,
   *  color: string,
   *  content: string
   * }>} notes – array of notes
   * @param noteIndexToBringToFront – index of the note to bring to front
   */
  setNotesOrder (notes, noteIndexToBringToFront) {
    if (!notes instanceof Array || !notes.length) return [];

    const newFrontNoteIndex = (
            typeof noteIndexToBringToFront === 'number' &&
            noteIndexToBringToFront >= 0 &&
            noteIndexToBringToFront < notes.length
          )
            ? noteIndexToBringToFront
            : notes.length - 1,
          newNotes = notes.map(note => note),
          noteToBringToFront = newNotes[newFrontNoteIndex],
          zIndexOfNoteToBringToFront = noteToBringToFront.zIndex || newFrontNoteIndex + 1;

    // Order notes here
    newNotes.forEach((note, index) => {
      const zIndex = note.zIndex || index + 1;
      note.zIndex = note.id === noteToBringToFront.id 
        ? newNotes.length
        : zIndex < zIndexOfNoteToBringToFront
          ? zIndex
          : zIndex - 1;

      // And focus the front note
      note.focus = note.id === noteToBringToFront.id;
    });

    return newNotes;
  }
  
  bringToFront (stickyNoteIndex) {
    this.setState(state => {
      return {
        notes: this.setNotesOrder(state.notes, stickyNoteIndex)
      };
    })
  }

  addNote (note) {
    this.setState(state => {
      const newNotes = state.notes.map(note => note)
      newNotes.push(note)
      return { notes: this.setNotesOrder(newNotes) }
    })
  }

  removeNote (noteToRemove) {
    this.setState(state => {
      const newNotes = state.notes.filter(note => note.id !== noteToRemove.id);
      return { notes: newNotes }
    })
  }
  
  updateNote (noteToUpdate) {
    this.setState(state => {
      const newNotes = state.notes.map(note => note);
      for (let i = 0; i < newNotes.length; i++) {
        if (newNotes[i].id === noteToUpdate.id) {
          newNotes[i] = noteToUpdate;
          break;
        }
      }
      return { notes: newNotes };
    })
  }

  toggleCreateNoteMode () {
    this.setState(state => {
      return {
        isCreateNoteModeActive: !state.isCreateNoteModeActive
      }
    });
  }

  onStartedDraggingNote (note) {
    this.setState({ noteThatIsBeingDragged: note || null })
  }

  onDraggingNote (x, y) {
    this.setState({ canDeleteNote: y > window.innerHeight * 0.7 })
  }

  onEndedDraggingNote (note) {
    this.setState(state => {
      if (state.canDeleteNote && state.noteThatIsBeingDragged) this.removeNote(state.noteThatIsBeingDragged);
      return {
        noteThatIsBeingDragged: null,
        canDeleteNote: false
      }
    })
  }

  handleMouseDownOnOverlay (event) {
    event && typeof event.preventDefault === 'function' && event.preventDefault();
    this.isDrawingANote = true;
    this.dragStartedAtX = event.pageX;
    this.dragStartedAtY = event.pageY;
    this.setState({
      newStickyNote: StickyNotes.createNote(event.pageX, event.pageY, 0, 0)
    });
  }

  handleMouseMoveOnOverlay (event) {
    event.preventDefault();
    if (!this.isDrawingANote || !this.state.newStickyNote) return;
    var _this = this;
    this.setState(state => {
      const resizedNewStickyNote = Object.assign({}, state.newStickyNote, {
        x: Math.min(_this.dragStartedAtX, event.pageX),
        y: Math.min(_this.dragStartedAtY, event.pageY),
        width: Math.abs(_this.dragStartedAtX - event.pageX),
        height: Math.abs(_this.dragStartedAtY - event.pageY),
      });
      return { newStickyNote: resizedNewStickyNote };
    });
  }

  handleMouseUpOnOverlay (event) {
    this.isDrawingANote = false;
    this.toggleCreateNoteMode();
    this.addNote(this.state.newStickyNote);
    this.setState({ newStickyNote: null });
  }

  onEndedStartAnimation () {
    this.setState({ isAnimatingAddNoteBtn: false });
  }

  async saveOnApi () {
    let notes;
    this.setState(state => {
      notes = state.notes;
      return { isSavingOnApi: true }
    });
    const response = await API.saveNotes(notes);
    console.log('Saved notes on API. Response:', response); // Just printing 
    this.setState({ isSavingOnApi: false });
  }

  async loadFromApi () {
    this.setState({ isLoadingFromApi: true });
    const loadedNotes = await API.loadNotes();
    console.log('Loaded from API:', loadedNotes); // Just in demonstration
    this.setState({
      isLoadingFromApi: false,
      notes: this.setNotesOrder(loadedNotes)
    });
  }

  render () {
    // Save notes on each update of the state
    Storage.save('stickyNotes', this.state.notes);

    return (
      <div className={`StickyNotes${this.state.isCreateNoteModeActive ? ' StickyNotes--is-create-mode' : ''}`}>
        <div className="StickyNotes__wrapper">
          {this.state.notes.map((stickyNote, index) =>
            <StickyNote
              key={stickyNote.id}
              model={stickyNote}
              isReadyToDelete={this.state.canDeleteNote && this.state.noteThatIsBeingDragged.id === stickyNote.id}
              onClick={this.bringToFront.bind(this, index)}
              onUpdate={this.updateNote.bind(this)}
              onStaredDragging={this.onStartedDraggingNote.bind(this)}
              onDragging={this.onDraggingNote.bind(this)}
              onEndedDragging={this.onEndedDraggingNote.bind(this)}
            />
          )}
        </div>
        <DeleteArea
          isShown={!!this.state.noteThatIsBeingDragged}
          zIndex={this.state.noteThatIsBeingDragged ? this.state.noteThatIsBeingDragged.zIndex : 0}
          isReadyToDelete={this.state.canDeleteNote}
        />
        <div
          className="StickyNotes__overlay"
          ref={this.overlayRef}
          onMouseDown={this.handleMouseDownOnOverlay.bind(this)}
          onMouseMove={this.handleMouseMoveOnOverlay.bind(this)}
          onMouseUp={this.handleMouseUpOnOverlay.bind(this)}
        >
          {this.state.newStickyNote &&
            <StickyNote model={this.state.newStickyNote} isOutlined />
          }
        </div>
        <button
          className={`
            StickyNotes__add-btn
            ${!this.state.notes.length ? ' StickyNotes__add-btn--is-accented' : ''}
            ${this.state.isAnimatingAddNoteBtn ? ' StickyNotes__add-btn--is-being-animated' : ''}
          `}
          onClick={this.toggleCreateNoteMode.bind(this)}
          onAnimationEnd={this.onEndedStartAnimation.bind(this)}
        >+</button>
        <div className="StickyNotes__add-btn-caption">Now create a new Sticky Note by drag'n'drop</div>

        <div
          className={`
            StickyNotes__api-btns-wrap
            ${this.state.notes.length ? '' : 'StickyNotes__api-btns-wrap--is-hidden'}
          `}
        >
          <button
            className="StickyNotes__api-btn StickyNotes__save-btn"
            disabled={this.state.isSavingOnApi || this.state.isLoadingFromApi}
            onClick={this.saveOnApi.bind(this)}
          >
            {this.state.isSavingOnApi
              ? <i className="spinner"></i>
              : 'Save on API'
            }
          </button>

          <button
            className="StickyNotes__api-btn StickyNotes__load-btn"
            disabled={this.state.isSavingOnApi || this.state.isLoadingFromApi}
            onClick={this.loadFromApi.bind(this)}
          >
            {this.state.isLoadingFromApi
              ? <i className="spinner"></i>
              : 'Load from API'
            }
          </button>
        </div>
      </div>
    );
  }
}

export default StickyNotes;