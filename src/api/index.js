import Storage from './../storage'
import { getRandomInt } from './../util/random'

class API {
  static loadNotes () {
    return new Promise(resolve => setTimeout(() => {
      resolve(Storage.load('stickyNotes', []));
    }, getRandomInt(300, 1500)));
  }

  static saveNotes (notes) {
    return new Promise(resolve => setTimeout(() => {
      resolve(Storage.save('stickyNotes', notes));
    }, getRandomInt(300, 1500)));
  }
}

export default API