class Storage {
  static load (key, emptyValue) {
    try {
      return JSON.parse(window.localStorage.getItem(key)) || emptyValue;
    } catch (error) {
      return typeof emptyValue === 'undefined'
        ? null
        : emptyValue;
    }
  }

  static save (key, notesData) {
    window.localStorage.setItem(key, JSON.stringify(notesData));
  }
}

export default Storage;