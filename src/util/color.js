import { getRandomInt } from './random'

class Color {
  static generateRandom (hmin, hmax, smin, smax, vmin, vmax) {
    return `hsl(${getRandomInt(hmin, hmax)}, ${getRandomInt(smin, smax)}%, ${getRandomInt(vmin, vmax)}%)`;
  }

  /**
   * Better use this function 'couse it generates somewhat
   * muted colors 
   */
  static generatePseudoRandom () {
    return Color.generateRandom(0,360, 60,100, 40,60);
  }
}

export default Color;